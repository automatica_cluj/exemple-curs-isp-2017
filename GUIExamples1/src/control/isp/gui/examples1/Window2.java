package control.isp.gui.examples1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by evo on 5/4/2017.
 */
public class Window2 extends JFrame implements ActionListener {
    JButton b = new JButton("Press me!");
    JButton b1 = new JButton("Press me too!");
    JTextField t = new JTextField("   ");
    int k;

    Window2(){
        setTitle("First window!");
        setSize(300,100);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        b.setBounds(10,10,100,50);
        this.add(b);
        this.add(b1);
        this.add(t);

        b.addActionListener(this); //implementare locala
        b1.addActionListener(this);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==b){
            k++;
            t.setText(""+k);

        }
        else if(e.getSource()== b1){
            k--;
            t.setText(""+k);
        }
        else{

        }
    }


}

