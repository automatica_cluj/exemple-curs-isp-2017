package control.isp.gui.examples1;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window1 extends JFrame implements ActionListener {
    JButton b = new JButton("Press me!");
    JTextField t = new JTextField("   ");
    int k;

    Window1(){
        setTitle("First window!");
        setSize(300,100);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        b.setBounds(10,10,100,50);
        this.add(b);
        this.add(t);
        b.addActionListener(new MyListener1()); //clasa interna
        b.addActionListener(new MyListener2(t)); //clasa externa
        b.addActionListener(this); //implementare locala
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                k++;
                t.setText(""+k);
            }
        });
        setVisible(true);
        }

    @Override
    public void actionPerformed(ActionEvent e) {
        k++;
        t.setText(""+k);
    }

    class MyListener1 implements ActionListener{
        int k = 0;
        @Override
        public void actionPerformed(ActionEvent e) {
            k++;
            t.setText(""+k);
            System.out.println("Button pressed!");

        }
    }

}

//clasa externa
class MyListener2 implements ActionListener{

    JTextField x;

    public MyListener2(JTextField x) {
        this.x = x;
    }

    int k = 0;
    @Override
    public void actionPerformed(ActionEvent e) {
        k++;
        x.setText(""+k);
        System.out.println("Button pressed!");

    }
}




