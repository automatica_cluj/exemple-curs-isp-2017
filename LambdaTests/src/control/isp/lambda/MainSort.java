package control.isp.lambda;

import java.util.Arrays;
import java.util.Comparator;

public class MainSort {
    public static void main(String[] args) {
        String words[] = {"hjgsd","sg","qytrwye","iuaslkdmoa","g"};

        Arrays.sort(words, new LengthComparator());
        System.out.println(Arrays.toString(words));

        Comparator<String> comp
                = (first,second) -> Integer.compare(first.length(),second.length());
        Arrays.sort(words, comp);

        Arrays.sort(words,(first,second) -> Integer.compare(first.length(),second.length()));


    }
}

class LengthComparator implements Comparator<String> {
    public int compare(String first, String second) {
        return Integer.compare(first.length(), second.length());
    }
}