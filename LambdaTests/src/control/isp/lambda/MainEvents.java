package control.isp.lambda;


import javafx.event.EventHandler;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainEvents {
    public static void main(String[] args) {
        UI1 u = new UI1();

    }
}

class UI1 extends JFrame {
    JButton b = new JButton("OK");
    UI1() {
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("Thanks for clicking!");
            }
        });
        this.getContentPane().add(b);
        setSize(200, 200);
        setVisible(true);
    }
}

class UI2 extends JFrame {
    JButton b = new JButton("OK");
    UI2() {
        b.addActionListener(event -> System.out.println("Thanks for clicking!"));
        this.getContentPane().add(b);
        setSize(200, 200);
        setVisible(true);
    }
}
