package control.isp.lambda;

public class MainThreads {

    public static void main(String[] args) {
        //metoda 1
        Thread t1 = new Thread(new Worker());
        t1.start();

        //metoda 1 expresii lambda
        Runnable r = () -> System.out.println("Do something else ...");
        new Thread(r).start();

        //metoda 3 expresii lambda
        new Thread(()-> System.out.println("Do something new ...")).start();
    }
}

class Worker implements Runnable {
    public void run() {
        System.out.println("Do something ...");
    }
}