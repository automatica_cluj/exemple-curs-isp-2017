/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank2;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author evo
 */
public class AccountsManagerTest {
    
    public AccountsManagerTest() {
    }

    @Test
    public void testLogin() {
        AccountsManager am =  new AccountsManager();
        try {
            am.login("123");
        } catch (BankSecurityException ex) {
            Logger.getLogger(AccountsManagerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals("user1", am.getCurrentUser());
    }

//    @Test
//    public void testLogout() {
//    }
//
//    @Test
//    public void testDeposit() {
//    }
//
//    @Test
//    public void testWithdraw() {
//    }
    
}
