/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 *
 * @author evo
 */
public class BankController implements ActionListener, Observer {

    private LoginUI loginUI;
    private MainWindowUI mainUI;
    private AccountsManager accManager;

    public BankController(LoginUI loginUI, MainWindowUI mainUI, AccountsManager accManager) {
        this.loginUI = loginUI;
        this.mainUI = mainUI;
        this.accManager = accManager;
        
        loginUI.jbLogin.addActionListener(this);
        mainUI.jButton1.addActionListener(this);
        mainUI.jButton2.addActionListener(this);
        mainUI.jButton3.addActionListener(this);        
        accManager.addObserver(this);
        
        loginUI.setVisible(true);
    }
   
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton b = (JButton)e.getSource();
        if(b.getText().equalsIgnoreCase("Login")){
            String pin = loginUI.jtfPin.getText();
            try {
                accManager.login(pin);
                loginUI.setVisible(false);
                mainUI.setVisible(true);
                populateMainWindow();
            } catch (BankSecurityException ex) {
                JOptionPane.showMessageDialog(loginUI," Invalid login!");//, pin, 0);
            }
        }
        if(b.getText().equalsIgnoreCase("Logout")){
            accManager.logout();
            clearMainWindow();
            mainUI.setVisible(false);
            loginUI.setVisible(true);
        }
        if(b.getText().equalsIgnoreCase("Deposit")){
            Double d = Double.parseDouble(mainUI.jTextField3.getText());
            accManager.deposit(d); 
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        populateMainWindow();
    }
    
    private void populateMainWindow(){
        mainUI.jTextField1.setText(accManager.getCurrentUser());
        mainUI.jTextField2.setText(""+accManager.getBalance());
    }
    
     private void clearMainWindow(){
        mainUI.jTextField1.setText("");
        mainUI.jTextField2.setText("");
    }
    
    
}
