
package bank2;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountsManager extends Observable {
    HashMap<String,String> users = new HashMap<>();
    HashMap<String,Double> deposits = new HashMap<>();
    String currentUser;
    
    public AccountsManager() {
        users.put("123", "user1");
        users.put("564", "user2");
        
        deposits.put("user1", new Double(15000));
        deposits.put("user2", new Double(30000));
        OperationsSimulator sim = new OperationsSimulator();
        sim.start();
    }
    
    public String login(String pin) throws BankSecurityException{
        if(users.containsKey(pin)){
            currentUser = users.get(pin);
            return users.get(pin);
        }
        else
            throw new BankSecurityException("Pin incorrect!");
    }
    
    public void logout(){
        currentUser = null;
    }
    
    public String getCurrentUser(){
        return currentUser;
    }
    
    public Double getBalance(){
        return deposits.get(this.currentUser);
    }
    
    public void deposit(double amount){
        if(currentUser!=null){
            double d = deposits.get(currentUser);
            deposits.put(currentUser, d+amount);
            setChanged();
            notifyObservers(currentUser);
        }
    }
    
    public void withdraw(double amount){
        if(currentUser!=null){
             double d = deposits.get(currentUser);
             if(d>=amount){
                 deposits.put(currentUser, d-amount);
                  setChanged();
                  notifyObservers(currentUser);
             }
        }
    }
    
    class OperationsSimulator extends Thread{
        public void run(){
            while(true){
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(AccountsManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            Random r = new Random();
            int i = r.nextInt(10000);
            deposit(i);
            }
        }
    }
    
    
}
