package control.isp.fire1;

/**
 * Created by evo on 5/11/2017.
 */

class MyT3 extends Thread{
    int k;
    Thread x;

    public MyT3(Thread x) {
        this.x = x;
    }

    public void run(){
        if(x!=null)
            try {
                x.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        //logica aplicatie
        while(k<5){
            k++;
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread "+getName()+" is running!");
        }
    }
}

public class ExempluJoin {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("START");
        MyT3 t1 = new MyT3(null);
        MyT3 t2 = new MyT3(t1);
        t1.start();
        //t1.join();
        t2.start();


    }
}
