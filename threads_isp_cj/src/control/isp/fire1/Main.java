package control.isp.fire1;

class MyT1 extends Thread{
    int k = 5;
    public void run(){
        //logica aplicatie
        while(k<5){
            k++;
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread "+getName()+" is running!");
        }
    }
}

class MyT2 implements Runnable{

    int k;
    private Thread t;

    public void start(){
        if(t==null){
            t = new Thread(this);
            t.setDaemon(true);
            t.start();
        }
    }

    public void run(){
        //logica aplicatie

        while(k<5){
            k++;
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread "+Thread.currentThread().getName()+" is running!");
        }
    }
}

public class Main {

    public static void main(String[] args) throws InterruptedException {
	    MyT1 t1 = new MyT1();
        MyT2 t2 = new MyT2();
        t1.setDaemon(true);
        t1.start();
        t2.start();
        Thread t3 = new Thread(new MyT2());
        t3.setDaemon(true);
        t3.start();
    }
}
