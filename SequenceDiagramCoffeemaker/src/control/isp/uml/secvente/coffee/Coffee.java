package control.isp.uml.secvente.coffee;

/**
 * Created by evo on 4/6/2017.
 */
public class Coffee {
    private int w;
    private int c;

    public Coffee(int w, int c) {
        this.w = w;
        this.c = c;
    }

    void drink(){
        System.out.println("Drink coffee [coffeine"+c+" with "+w+" water]");
    }
}
