package control.isp.uml.secvente.coffee;

/**
 * Created by evo on 4/6/2017.
 */
public class WaterTank extends IngredientsTank {
    int temp;

    public WaterTank(int q, int temp) {
        super(q);
        this.temp = temp;
    }


    @Override
    int getIngredient() {
        if(getQ()>50)
            setQ(getQ()-50);
        return 50;
    }
}
