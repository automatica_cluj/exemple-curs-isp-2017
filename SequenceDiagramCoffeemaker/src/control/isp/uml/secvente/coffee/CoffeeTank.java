package control.isp.uml.secvente.coffee;

/**
 * Created by evo on 4/6/2017.
 */
public class CoffeeTank extends IngredientsTank {

    private String name;

    public CoffeeTank(int q, String name) {
        super(q);
        this.name = name;
    }

    @Override
    int getIngredient() {

        if(getQ()>10)
            setQ(getQ()-10);
        return 10;
//        if(q>10)
//            q-=10;
//        return 10;
    }
}
