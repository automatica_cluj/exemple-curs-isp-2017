package control.isp.uml.secvente.coffee;

public abstract class IngredientsTank {

    private int q; //cantitate disponibila

    public IngredientsTank(int q) {
        this.q = q;
    }

    abstract int getIngredient();

    public int getQ() {
        return q;
    }

    public void setQ(int q) {
        this.q = q;
    }
}
