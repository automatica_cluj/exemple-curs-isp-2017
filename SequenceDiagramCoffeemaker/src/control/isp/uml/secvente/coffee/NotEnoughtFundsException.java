package control.isp.uml.secvente.coffee;

/**
 * Created by evo on 4/6/2017.
 */
public class NotEnoughtFundsException extends Exception {

    int v;

    public NotEnoughtFundsException(String message, int v) {
        super(message);
        this.v = v;
    }


}
