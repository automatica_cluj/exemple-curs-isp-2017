package control.isp.uml.secvente.coffee;

/**
 * Created by evo on 4/6/2017.
 */
public class UserInterface {
    private Controler ctr;
    private int value;

    public UserInterface(Controler ctr) {
        this.ctr = ctr;
    }

    public void pay(int v){
        this.value = v;
    }

    public Coffee placeOrder() throws NotEnoughtFundsException{
        if(value>=15){
            System.out.println("Prepare coffee!");
            Coffee c = ctr.executeOrder();
            value = 0;
            System.out.println("Coffeee ready!");
            return c;
        }else{
            throw new NotEnoughtFundsException("Current funds "+value+" not enought!",value);
            //System.out.println("Not engouth funds!");
            //return null;
        }

    }



}

