package control.isp.uml.secvente.coffee;

/**
 * Created by evo on 4/6/2017.
 */
public class Controler {

    private WaterTank wt;
    private CoffeeTank ct;

    public Controler(WaterTank wt, CoffeeTank ct) {
        this.wt = wt;
        this.ct = ct;
    }

    public Coffee executeOrder(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int w = wt.getIngredient();
        int c = ct.getIngredient();
        return new Coffee(w,c);
    }
}
