package control.isp.uml.secvente.coffee;

/**
 * Created by evo on 4/6/2017.
 */
public class CoffeeMaker {
    WaterTank wt;
    CoffeeTank ct;
    UserInterface ui;
    Controler ctrl;

    public CoffeeMaker() {
        wt = new WaterTank(100,70);
        ct = new CoffeeTank(100,"XYZ");
        ctrl = new Controler(wt,ct);
        ui = new UserInterface(ctrl);
    }

    public UserInterface getUserInterface(){
        return ui;
    }
}
