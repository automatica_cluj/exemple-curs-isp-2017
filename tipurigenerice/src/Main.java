public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        Node<Integer> n1 = new Node<Integer>();
    }
}

class Node<T>{
    private T value;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
