package isp.dp.proxy;

public class Client {
    public static void main(String[] args) {
        Proxy opsProxy = new Proxy();
        opsProxy.action2();
        opsProxy.action1();
    }
}

interface Operations{
    void action1();
    void action2();
}

class RealOperations implements Operations{
    @Override
    public void action1() {

        System.out.println("Do action 1");
    }

    @Override
    public void action2() {
        System.out.println("Do action 2");
    }
}

class Proxy implements Operations{

    private RealOperations real;

    Proxy(){
        real = new RealOperations();
    }


    @Override
    public void action1() {
        real.action1();
    }

    @Override
    public void action2() {
        real.action2();
    }
}
