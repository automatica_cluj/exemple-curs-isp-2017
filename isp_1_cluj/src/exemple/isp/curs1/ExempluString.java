package exemple.isp.curs1;

/**
 * Created by evo on 3/9/2017.
 */
public class ExempluString {
    public static void main(String[] args) {
        String s1 = "abc";
        String s2 = new String("123");

        //concatenarea a doua string-uri
        String s3 = s1 + s2;
        System.out.println(s3);

        //concatenare string cu intreg
        s2 = s1 +45;

        System.out.println(s2);

        //lungimea unui string
        System.out.println(s2.length());

        //exemplu indexof
        if(s3.indexOf("12")!=-1){
            System.out.println("Substringul existsa");
        }else{
            System.out.println("Nu exista");
        }

        //comparare corecta a doua string-uri
        if(s1.equals(s2)){
            // ...
        }

        //comparare a doua string-uri cu ignorearea upper lower case
        if(s1.equalsIgnoreCase(s2)){
            // ...
        }

        if(s1 == s2){ //INCORECT

        }

    }
}
