package exemple.isp.curs1;

import java.util.Arrays;

/**
 * Created by evo on 3/9/2017.
 */
public class ExempluArray {
    public static void main(String[] args) {

        //declarare si initializare array
        int[] a = new int[10];

        //parcurgere array
        for (int i = 0; i < a.length; i++) {
            System.out.println("a["+i+"]="+a[i]);
        }

        //foreach
        for(int x:a){
            System.out.println(x);
        }


        //declarare array
        int[] b;

        //si initializare ulterioara
        b = new int[4];

        //accesare element din array
        System.out.println(a[12]);

        //array bidimensional
        int c[][] = new int[10][23];

        //array bidimensional
        int d[][] = new int[3][];
        d[0] = new int[2];
        d[1] = new int[5];
        d[3] = new int [1];

        int[] e = {1,4,2};





    }
}
