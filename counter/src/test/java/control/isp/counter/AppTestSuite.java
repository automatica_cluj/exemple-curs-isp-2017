package control.isp.counter;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        CounterTest.class,
})

public class AppTestSuite {
}