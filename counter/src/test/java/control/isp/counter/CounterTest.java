package control.isp.counter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CounterTest {

    private Counter c;

   @Before
   public void initTest(){
       System.out.println("Initialize test");
       c = new Counter();
   }

   @Test
   public void testInc(){
       System.out.println("Executing test 1");
       c.inc();
       c.inc();
       assertEquals(c.getValue(),2);
   }

    @Test
    public void testSetValue(){
        System.out.println("Executing test 2");
        c.setValue(1);
        assertEquals(c.getValue(),1);
    }
}
