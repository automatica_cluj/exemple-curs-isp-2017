package control.isp.counter;

/**
 * Created by evo on 3/28/2017.
 */
public class Counter {

    private int k = 0;

    public void inc(){
        k++;
    }

    public void setValue(int k){
        this.k = k;
    }

    public int getValue(){
        return k;
    }
}
