package control.isp.counter;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Counter c1 = new Counter();
        c1.inc();
        c1.inc();
        c1.inc();
        System.out.println( "Counter value: "+c1.getValue() );
    }
}
