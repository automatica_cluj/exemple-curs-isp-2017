package control.isp.curs2sm;

public class Main {

    public static void main(String[] args) {
	    Robot r1 = new Robot();
        Robot r2 = new Robot();
        r1.x = 9;
        r2.x = 10;

        Robot2 r3 = new Robot2();
        r3.moveLeft();
        r3.moveLeft();

        Robot2 r4 = new Robot2();
        r4.moveRight();
        r4.moveRight();
        r4.moveLeft();

        Robot3 r5 = new Robot3();
        Robot3 r6 = new Robot3();
        r5.moveRight();
        r5.moveRight(10);

        //accesare continut static
        r5.Y = 77;
        r6.Y = 99;
        Robot3.Y = 88;
        System.out.println(r5.Y);

        Robot4 a = new Robot4();
        Robot4 b = new Robot4(19);
        a.moveRight(4);


    }
}

//atribute
class Robot{
    int x;
}

//atribute si metode
class Robot2{
    private int x;

    void moveLeft(){
        if(x>0)
            x--;
    }

    void moveRight(){
        if(x<100)
            x++;
    }
}

class Robot3{
    private int x;
    public static int Y;

    void moveLeft(){
        if(x>0)
            x--;
    }

    void moveRight(){
        if(x<100)
            x++;
    }

    //supraincarcarea (overload) metodelor
    void moveRight(int k){
        if((x+k)<100)
            x = x+k;
    }
}

//constructori si utilizarea cuvantului cheie this
class Robot4{
    private int x;

    Robot4(){
        this(10);
        //x = 10; // ca varianta alternativa la this(10) - se recomanda varianta cu this
    }

    public Robot4(int x) {
        this.x = x;
    }

    void moveLeft(){
        if(x>0)
            x--;
    }

    void moveRight(){
        if(x<100)
           this.moveRight(1);
    }

    //supraincarcarea (overload) metodelor
    void moveRight(int k){
        if((x+k)<100)
            x = x+k;
    }
}
