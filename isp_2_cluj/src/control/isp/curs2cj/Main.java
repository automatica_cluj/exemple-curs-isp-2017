package control.isp.curs2cj;

public class Main {

    public static void main(String[] args) {
        System.out.println();
       Bicycle2 b1 = new Bicycle2(1,1);
        Bicycle2 b2 = new Bicycle2(1,1);

       //compararea corecta a obiectelor se face cu equals
        if(b1.equals(b2)){
            System.out.println("Egale");
        }
        else
            System.out.println("Nu sunt egale");


       // Bicycle2 b3 = new Bicycle2(3);
        //Bicycle2 b4 = new Bicycle2();

       // b1.afiseaza();
       // b2.afiseaza();

//        b1.setGear(1);
//        b1.setSpeed(1);
//        b2.setGear(2);
//        b2.setSpeed(1);


//      Partea 3 - comportamentul cuvantului cheie static
//        Bicycle b1 = new Bicycle();
//        Bicycle b2 = new Bicycle();
//
//        System.out.println(Bicycle.FACTORY);
//        b1.incrementGear();
//        b2.incrementGear();
//        b1.accelerate();
//        b2.accelerate();
//        b2.accelerate();
//        b2.accelerate();
//
//        b1.afiseaza();
//        b2.afiseaza();

 // Partea 2 - apealarea metodelor
//        b1.incrementGear();
//        b2.incrementGear(3);
//        for (int i = 0; i <100 ; i++) {
//            b1.accelerate();
//            System.out.println(b1.getSpeed()+" in gear "+b1.getGear());
//        }


        /*
        Partea 1 - accesarea atributelor
        b1.speed = 10;
        b1.gear = 2;

        b2.speed = 15;
        b2.gear = 3;
        System.out.println("B1 speed="+b1.speed+" gear="+b1.gear);
        System.out.println("B2 speed="+b2.speed+" gear="+b2.gear);

        Bicycle b3;

        b3 = b1;
        b2 = b1;
        System.out.println("B1 speed="+b1.speed+" gear="+b1.gear);
        System.out.println("B2 speed="+b2.speed+" gear="+b2.gear);

        b3.speed = -100;
        */
    }
}
