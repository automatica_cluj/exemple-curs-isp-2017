package control.isp.curs2cj;

/**
 * Exemplificare equals si suprascrierea constructorilor
 */
public class Bicycle2 {
    private int speed;
    private int gear;

    //suprascrierea constructorilor

    public Bicycle2(int speed) {
        this(speed,1);
        this.speed = speed;
    }

    public Bicycle2(int speed, int gear) {
        System.out.println("Apelare constructor");
        this.speed = speed;
        this.gear = gear;
    }


    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    void afiseaza() {
        System.out.println("B speed " + this.speed + " gear " + this.gear);
    }

    @Override
    public boolean equals(Object obj) {
        Bicycle2 x = (Bicycle2)obj; //conversie de tip
        return x.gear==gear&&x.speed==speed;
    }
}
