package control.isp.curs2cj.forme;



import java.util.Random;


/**
 * Exemplificare concept polimorfism.
 */
public class Desen {

    Punct[] forme = new Punct[10];

    Desen(){
        Random r = new Random();

        for (int i = 0; i <forme.length ; i++) {
            int x = r.nextInt(10);
            if(x<5){
                forme[i] = new Cerc(r.nextInt(20),r.nextInt(20),r.nextInt(20));
            }else{
                forme[i] = new Punct(r.nextInt(20),r.nextInt(20));
            }
        }

    }

    void deseneaza(){
        for(Punct p:forme){
            p.afiseaza();
        }
    }

    public static void main(String[] args) {
        Desen d = new Desen();
        d.deseneaza();
    }

}
