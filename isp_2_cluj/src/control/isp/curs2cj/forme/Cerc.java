package control.isp.curs2cj.forme;


public class Cerc extends Punct{
    private int r;

    public Cerc(int x, int y, int r) {
        super(x, y);
        this.r = r;
    }

    @Override
    void afiseaza() {
        System.out.println("[Cerc "+r+"]");
        super.afiseaza();
    }
}
