package control.isp.curs2cj.telefon1;

/**
 * Exemplificare construire aplicatie prin agregare.
 */
public class Telefon {

    private Baterie b;

    public Telefon(Baterie b) {
        this.b = b;
    }

    void apelare(){
        System.out.println("Apeleaza...");
        b.utilizare();
    }

    @Override
    public String toString() {
        return "Telefon mobil cu baterie "+b.level;
    }

    public static void main(String[] args) {
        Baterie b1 = new Baterie();
        Telefon t1 = new Telefon(b1);
        t1.apelare();
        t1.apelare();
        System.out.println(t1);

    }

}
