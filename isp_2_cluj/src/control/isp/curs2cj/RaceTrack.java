package control.isp.curs2cj;

import java.util.Random;

public class RaceTrack {
    Bicycle[] b = new Bicycle[10];

    RaceTrack(){
        Random r = new Random();
        for (int i = 0; i <b.length ; i++) {
            b[i] = new Bicycle();
            b[i].setGear(r.nextInt(10));
        }
    }

    void race(){
        for(int i = 0;i<100;i++) {
            for (Bicycle x : b) {
                x.accelerate();
            }
        }

        for (int i = 0; i < b.length; i++) {
            b[i].afiseaza();
        }

        //sortare
    }

    public static void main(String[] args) {
        RaceTrack t1 = new RaceTrack();
        t1.race();
    }


}
