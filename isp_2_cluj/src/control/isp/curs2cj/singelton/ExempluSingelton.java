package control.isp.curs2cj.singelton;

class Connection{

    private static Connection INSTANCE;

    private Connection(){}

    static  Connection getInstance(){
        if(INSTANCE==null)
            INSTANCE = new Connection();

        return INSTANCE;
    }


}

public class ExempluSingelton {

    public static void main(String[] args) {
        //Connection c1 = new Connection();
        //Connection c2 = new Connection();

        Connection c1 = Connection.getInstance();
        Connection c2 = Connection.getInstance();

    }
}
