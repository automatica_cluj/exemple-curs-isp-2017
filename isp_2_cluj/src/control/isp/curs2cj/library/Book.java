package control.isp.curs2cj.library;

/**
 * Exemplificare construire aplicatie prin compozitie.
 */
public class Book {

    Page[] pages = new Page[100];

    Book(){
        for (int i = 0; i < pages.length; i++) {
            pages[i] = new Page();
        }
    }

    void readPage(int i){
        pages[i].read();
    }

    public static void main(String[] args) {
        Book b1 = new Book();
        b1.readPage(45);
    }
}
