package control.isp.curs2cj;


class Bicycle {

    public static String FACTORY = "Focus";
    private int speed;
    private int gear;
    private int distance;

    void afiseaza(){
        System.out.println("B speed "+speed+" gear "+gear+" distance "+distance);
    }
    void accelerate(){
        if(speed>30&&gear<3)
            incrementGear();
        if(speed<80)
            speed=speed + 1*gear;
        else
            System.out.println("Viteza prea mare!");

        distance+=speed;
    }

    //overload
    void incrementGear(int steps){
        if(gear+steps<10)
            gear+=steps;
    }

    void incrementGear(){
        gear++;
    }

    void decrementGear(){
        gear--;
    }

    void setGear(int gear) {
        this.gear = gear;
    }

    public int getSpeed() {
        return speed;
    }

    public int getGear() {
        return gear;
    }
}
