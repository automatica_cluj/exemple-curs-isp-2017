package control.isp.tests.interfete;

/**
 * Created by evo on 3/20/2017.
 */
public interface IEngine {
    void start();
    void stop();
}
