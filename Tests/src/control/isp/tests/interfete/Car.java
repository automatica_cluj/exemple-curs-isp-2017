package control.isp.tests.interfete;

/**
 * Created by evo on 3/20/2017.
 */
public class Car {

    IEngine engine;

    public Car(IEngine engine) {
        this.engine = engine;
    }

    void startCar(){
        engine.start();
    }

    void stopCar(){
        engine.stop();
    }
}
