package control.isp.tests.interfete;

/**
 * Created by evo on 3/20/2017.
 */
public class DieselEngine implements IEngine {
    @Override
    public void start() {
        System.out.println("Start diesel engine.");
    }

    @Override
    public void stop() {
        System.out.println("Stop diesel engine");
    }
}
