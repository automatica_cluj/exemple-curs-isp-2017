package control.isp.tests.enumuri;

enum Day {
    MONDAY,SUNDAY;
}

public class Main {
    static void checkDay(Day d){
        switch(d){
            case MONDAY:{
                System.out.println(d.name()+" it's a working day!");
                break;
            }
            case SUNDAY:{
                System.out.println(d.name()+" it's a free day!");
                break;
            }
        }
    }

    public static void main(String[] args) {
        //checkDay(Day.MONDAY);
        //checkDay(Day.SUNDAY);
        Day[] days = Day.values();
        for(Day d:days)
            System.out.println(d);
        System.out.println(Day.valueOf("MONDAY"));
    }
}
