package control.isp.astract1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class UI extends JFrame  {

    JButton button = new JButton("Press");

    UI(){
        setTitle("Test tile");
        setSize(200,200);

        button.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Button pressed!!!");
            }
        });


        this.getContentPane().add(button);
        setVisible(true);
    }


}


abstract class GraphicObject {
    int x, y;
    void moveTo(int newX, int newY) { //metoda normala
        System.out.println("Move graphic object to position"+x+":"+y);
    }
    abstract void draw();                                         //metoda abstracta
}

interface GraphicObject2{
    int X_ORIGIN = 0;
    int Y_ORIGIN = 0;

    void moveTo(int newX, int newY);
    void draw();
}


interface Instrument {
    void test(){
        System.out.println("DEFAULT IMPLEMENTATION");
    };
}


//class Circle2 implements GraphicObject2, Instrument{
//
//    @Override
//    public void moveTo(int newX, int newY) {
//        System.out.println("Move graphic object to position"+x+":"+y);
//    }
//
//    @Override
//    public void draw() {
//        System.out.println("draw....");
//    }
//
//    @Override
//    public void test() {
//        System.out.println("DO A TEST");
//    }
//}

class Circle extends GraphicObject {
    void draw() {
        System.out.println("Draw circle");
    }
}

public class Test {
    public static void main(String[] args) {
        GraphicObject g1 = new Circle();
        g1.draw();
        g1.moveTo(5,6);

        Instrument a = new Instrument(){
            @Override
            public void test() {
                System.out.println("DO SOMETHING ELSE");
            }
        };

        a.test();

        UI ui1 = new UI();

        Instrument x = () -> System.out.println("asd");

        //new Instrument(()-> System.out.println("aaa")).test();

        //GraphicObject g2 = new GraphicObject(); //eroare
    }
}
