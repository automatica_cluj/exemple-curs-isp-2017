package control.isp.tests.bank1;

import java.util.ArrayList;
import java.util.Collections;

public class Bank {

    ArrayList<Customer> customers = new ArrayList<Customer>();

    void addCustomer(String id) {
        Customer c = new Customer(id);
        customers.add(c);
    }

    void deposit(String cid, int amount) {
        Customer temp = new Customer(cid);
        if(customers.contains(temp)) {
            customers.get(customers.indexOf(temp)).getAccount().deposit(amount);
            System.out.println("Operation deposit succesful!");
        }
        else
            System.out.println("No account found for cid:"+cid);
    }

    void print(){
        Collections.sort(customers);
        for(Customer c:customers)
            System.out.println(c.id+" "+c.account.balance);
    }

    public static void main(String[] args) {
        Bank bt = new Bank();
        bt.addCustomer("1A");
        bt.addCustomer("2B");
        bt.addCustomer("3C");
        bt.addCustomer("2B");
        bt.deposit("1A",100);
        bt.deposit("2B",50);
        bt.deposit("3C",70);

        bt.print();
    }
}

class Customer implements Comparable<Customer> {
    String id;
    BankAccount account;

    public Customer(String id) {
        this.id = id;
        account = new BankAccount(id, 0);
    }

    public BankAccount getAccount() {
        return account;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null) return false;
        if(!(obj instanceof Customer)) return false;
        return id.equals(((Customer)obj).id);
    }

    @Override
    public int compareTo(Customer c) {
        return (c.account.balance < account.balance) ? -1 : (c.account.balance > account.balance) ? 1 : 0;
    }
}


class BankAccount {
    String owner;
    int balance;

    public BankAccount(String owner, int balance) {
        this.balance = balance;
        this.owner = owner;
    }

    public void deposit(int amount) {
        balance += amount;
    }

    public void withdraw(int amount) {
        balance -= amount;
    }
}


