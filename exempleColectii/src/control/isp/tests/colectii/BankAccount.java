package control.isp.tests.colectii;


import java.util.Comparator;

public interface BankAccount extends Comparable<BankAccount> {
    void deposit(int amount);
    void withdraw(int amount);
    int getBalance();
    String getId();
}

class SavingAccount implements BankAccount{
    private String id;
    private int balance;

    public SavingAccount(String id, int balance) {
        this.balance = balance;
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void deposit(int amount) {
        balance+=amount;
    }

    @Override
    public void withdraw(int amount) {
        if(balance-amount>=0)
            balance-=amount;
    }

    public int getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object obj) {
       if(obj==null) return false;
       if(!(obj instanceof SavingAccount)) return false;
       SavingAccount x = (SavingAccount)obj;
       return x.id.equals(this.id);
    }

    @Override
    public String toString() {
        return id+":"+balance;
    }

    @Override
    public int compareTo(BankAccount o) {
        return (balance>o.getBalance())?-1:(balance<o.getBalance())?-1:0;
    }
}

class BankAccountNameSort implements Comparator<BankAccount> {

    @Override
    public int compare(BankAccount o1, BankAccount o2) {
        return o1.getId().compareTo(o2.getId());
    }
}
