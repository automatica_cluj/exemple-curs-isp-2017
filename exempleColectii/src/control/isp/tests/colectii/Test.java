package control.isp.tests.colectii;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

public class Test {
    public static void main(String[] args) {
        for(int row=1;row<=3;row++){
            for(int count = 1;count<=(4-row);count++)
                System.out.print("*");
            System.out.println();
        }

//        int sum =0;
//        int count = 5;
//        while(count>1){
//            sum = sum+count;
//            count = count +2;
//        }
//        System.out.println(":"+sum);
        if(true) return;
        ArrayList<BankAccount> acc = new ArrayList<>(); //declarare lista
        HashSet<BankAccount> acc2 = new HashSet<>();
        BankAccount a1 = new SavingAccount("A1",100);
        SavingAccount a2 = new SavingAccount("B2",300);

        acc.add(a1); //adaugare elemente in lista
        acc.add(a2);
        acc.add(a2);
        acc.add(a2);
        acc.add(a2);
        System.out.println("list size:"+acc.size());

        acc2.add(a1); //adaugare elemente in set
        acc2.add(a2);
        acc2.add(a2);
        acc2.add(a2);
        acc2.add(a2);
        System.out.println("set size:"+acc2.size());

        for(int i=0;i<acc.size();i++){ //parcurgere 1
            BankAccount b = acc.get(i);
            b.deposit(200);
            System.out.println(b);
        }

        for(BankAccount b:acc){ //parcurgere cu foreach
            b.deposit(90);
            System.out.println(b);
        }

        Iterator<BankAccount> i = acc.iterator(); //parcurgere cu iterator
        while(i.hasNext()){
            BankAccount b = i.next();
            System.out.println(b);
        }

        //BankAccount x = acc.remove(1);
        //System.out.println("Account sters:"+x);

        SavingAccount a3 = new SavingAccount("B2",300); //obiectul 3 va fi sters doar daca metoda equals este definita corect
        boolean y = acc.remove(a3);
        System.out.println("Account sters:"+y);

        //////////////soartare
        Collections.sort(acc);
        Iterator<BankAccount> i2 = acc.iterator();
        while(i2.hasNext()){
            BankAccount b = i2.next();
            System.out.println(b);
        }

        Collections.sort(acc, new BankAccountNameSort());
        Iterator<BankAccount> i3 = acc.iterator();
        while(i3.hasNext()){
            BankAccount b = i3.next();
            System.out.println(b);
        }
    }
}
