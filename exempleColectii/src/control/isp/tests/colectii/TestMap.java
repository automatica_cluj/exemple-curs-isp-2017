package control.isp.tests.colectii;

import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

public class TestMap {
    public static void main(String[] args) {
        HashMap<String,String> dictionar = new HashMap<>();

        dictionar.put("cuvant3","definitie3");
        dictionar.put("cuvant112","definitie1");
        dictionar.put("cuvant223","definitie2");
        dictionar.put("cuvant4232","definitie4");
        dictionar.put("cuvant1","definitie4");

        Iterator<String> i =  dictionar.keySet().iterator();
        while(i.hasNext()){
            String key = i.next();
            System.out.println("Key = "+key);
            String value = dictionar.get(key);
            System.out.println(key+":"+value);
        }

        boolean b = dictionar.containsKey("cuvant3");





    }
}
