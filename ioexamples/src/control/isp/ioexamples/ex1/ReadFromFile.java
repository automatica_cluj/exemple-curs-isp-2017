package control.isp.ioexamples.ex1;

import java.io.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadFromFile {


    public static void readFromFile2(){
        File f = new File("file1.txt");
        try(BufferedReader r = new BufferedReader(new FileReader(f))){
            String line = null;
            while((line=r.readLine())!=null){
                System.out.println(">"+line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readFromFile3(){
        try {
            String content = new String(Files.readAllBytes(Paths.get("file1.txt")));
            System.out.println(">"+content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readFromFile4(){
        try (BufferedReader reader =
                     Files.newBufferedReader(Paths.get("file1.txt"))) {
            String line = null;
            while((line = reader.readLine())!=null){
                System.out.println(">"+line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
