package control.isp.ioexamples.ex1;

import java.io.*;
import java.io.InputStreamReader;

/**
 * Created by evo on 4/27/2017.
 */
public class Extastatura {

    public static void main(String[] args) throws IOException {
       //int x =  System.in.read();
       BufferedReader br = new BufferedReader(
               new InputStreamReader(System.in)
       ) ;

       String s1 =  br.readLine();
       String s2 =  br.readLine();

        System.setOut(new PrintStream(new FileOutputStream("out.txt")));

        System.out.println(s1);
        System.out.println(s2);

        System.err.println("Hello");


    }
}
