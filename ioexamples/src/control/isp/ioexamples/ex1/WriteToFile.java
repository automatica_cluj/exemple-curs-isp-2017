package control.isp.ioexamples.ex1;

import java.io.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by evo on 4/26/2017.
 */
public class WriteToFile {

    //write to file
    public static void writeToFile1(){
        File f = new File("file1.txt");
        FileWriter fw = null;
        try {
            f.createNewFile();
            fw = new FileWriter(f);
            fw.write("Line 1\n");
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fw!=null)
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

    }

    //write to file try with resources
    public static void writeToFile2(){
        File f = new File("file1.txt");
        try(FileWriter fw = new FileWriter(f)){
            f.createNewFile();
            fw.write("Line 2\n");
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //write with buffer
    public static void writeToFile3(){
        File f = new File("file1.txt");
        try(BufferedWriter fw = new BufferedWriter(new FileWriter(f))){
            fw.write("Line 3");
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeToFile4(){
        Path path = Paths.get("file1.txt");
        try {
            Files.write(path, "Line 4".getBytes()); //creates, overwrites
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
