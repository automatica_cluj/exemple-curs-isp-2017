package control.isp.ioexamples.ex1;

public class Main {

    public static void main(String[] args) {
	    WriteToFile.writeToFile1();
        WriteToFile.writeToFile2();
        WriteToFile.writeToFile3();
        WriteToFile.writeToFile4();

        ReadFromFile.readFromFile2();
        ReadFromFile.readFromFile3();
        ReadFromFile.readFromFile4();
    }
}
