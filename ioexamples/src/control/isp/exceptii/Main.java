package control.isp.exceptii;

public class Main {

    void f1(boolean condition) throws MyException {
        if(condition)
            throw new MyException("Eroare!");
    }

    void f2(){
        try {
            f1(false);
        } catch (MyException e) {
            e.printStackTrace();
        }
    }

    void f3() throws MyException {
        f1(true);
    }

    public static void main(String[] args) {
        Main m1 = new Main();

        try {
            m1.f3();
        } catch (MyException e) {
           // e.printStackTrace();
            System.out.println("Eroare!");
        } finally{

        }

        m1.f2();
    }

}

class MyException extends Exception{
    MyException(String msg){
        super(msg);
    }
}