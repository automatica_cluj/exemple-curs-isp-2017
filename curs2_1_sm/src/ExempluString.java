/**
 * Created by evo on 3/4/2017.
 */
public class ExempluString {
    public static void main(String[] args) {
        String s1 = "Hello ";
        String s2 = "World";
        System.out.println(s1);
        System.out.println(s1+s2);
        String s3 = s1 + s2;
        String s4 = "Data "+10;

        int a1=123;
        String s5 = "Abc"+a1;
        String s6 = new String("Un text");

        if(s1.equals(s2))
            System.out.println("siruri egale");
        else
            System.out.println("siruri diferite");
    }
}
