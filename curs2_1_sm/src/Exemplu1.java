
public class Exemplu1 {
    public static void main(String[] args) {
        int x=7;
        int y = 10;
        boolean v1=false;
        boolean v2 = true;

        System.out.println("x="+x);
        System.out.println("v1="+v1);

        //1 widening
        byte a1 = (byte)178;
        int a2;
        a2 = a1;

        //2 narrowing
        int b1 = 7676373;
        byte b2;

        b2 = (byte)b1;
    }
}
