package control.isp.colectii;


import java.util.ArrayList;
import java.util.Iterator;

public class TestArrayList {
    public static void main(String[] args) {
        BankAccount[] a1 =new BankAccount[10];
        a1[0] = new BankAccount("Alin",10000);

        ArrayList a2 = new ArrayList();
        //adaugare
        a2.add(new BankAccount("Ion1",900));
        a2.add(new BankAccount("Ion2",1900));
        a2.add(new BankAccount("Ion3",2300));
        a2.add(new BankAccount("Ion4",4500));

        Object obj = a2.get(0);
        BankAccount x1 = (BankAccount)obj; //conversie de tip
        x1.print();

        BankAccount x2 = (BankAccount)a2.get(1);
        x2.print();

        //
        ArrayList<BankAccount> a3 = new ArrayList<>();
        a3.add(new BankAccount("Ion1",900));
        a3.add(new BankAccount("Ion2",1900));
        a3.add(new BankAccount("Ion3",2300));

        BankAccount y1 = a3.get(0);
        y1.print();

        for(int i = 0;i<a3.size();i++) //for clasic
            a3.get(i).print();

        for(BankAccount b:a3) //foreach
            b.print();

        Iterator<BankAccount> it = a3.iterator(); //iterator
        while(it.hasNext()){
            BankAccount z = it.next();
            x1.print();
        }

    }
}

class BankAccount{
    private String owner;
    private int balance;

    public BankAccount(String owner, int balance) {
        this.balance = balance;
        this.owner = owner;
    }

    void print(){
        System.out.println("Bank account:"+balance);
    }
}
