package control.isp.colectii;
import java.util.*;

public class TestHashMap {

    public static void main(String[] args) {
        HashMap<String,String>
                dictionar = new HashMap<>();

        dictionar.put("cuvant1","definitie1");
        dictionar.put("cuvant2","definitie2");
        dictionar.put("cuvant3","definitie3");
        dictionar.put("cuvant4","definitie4");

        //verificare cheie
        if(dictionar.containsKey("cuvant1")){
            System.out.println("Cheia exista");
        }

        //stergere
        dictionar.remove("cuvant2");

        //parcurgere
        Iterator<String> it = dictionar.keySet().iterator();
        while(it.hasNext()){
            String key = it.next();
            String value = dictionar.get(key);
            System.out.println(key+"->"+value);
        }
    }
}
