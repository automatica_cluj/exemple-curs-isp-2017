package control.isp.colectii;

/**
 * Exemplu comparare corecta a obiectelor.
 */
public class TestFlower {
    public static void main(String[] args) {
        Flower f1 = new Flower("Lalea",5);
        Flower f2 = new Flower("Lalea",5);

        //gresit
        //if(f1==f2){
        //    System.out.println("Florile sunt identice");
        //}

        if(f1.equals(f2)){
            System.out.println("Florile sunt identice");
        }else{
            System.out.println("Florile sunt diferite");
        }


    }
}


class Flower {
    private int petals;
    private String name;

    public Flower(String name, int petals) {
        this.petals = petals;
        this.name = name;
    }

    void show() {
        System.out.println("Flower " + name + " number of petals " + petals);
    }



    public int hashCode(){
        return petals;
    }

    public boolean equals(Object obj) {
        if(obj==null)
            return false;
        if(!(obj instanceof Flower))
            return false;
        Flower q = (Flower)obj;
        if(petals==q.petals && name.equals(q.name))
            return true;
        else
            return false;
    }
}
