package control.isp.agergare;


public class MainCar {
    public static void main(String[] args) {
        Engine e1 = new Engine();
        Car c1 = new Car(e1);

        c1.accelerate();
        c1.startStopCar();
        c1.accelerate();
        c1.accelerate();
        c1.startStopCar();
        c1.accelerate();

    }
}

class Car{
    private Engine e;
    private int speed;

    public Car(Engine e) {
        this.e = e;
        this.speed = 0;
    }

    public void startStopCar(){
        e.startStop();
        if(e.isStarted()==false)
            speed = 0;
    }

    public void accelerate(){
        if(this.e.isStarted()){
            speed++;
            System.out.println("Car speed is "+speed);
        }else
            System.out.println("Engine is not started.");
    }

}

class Engine{
    private boolean started;

    void startStop(){
        started=!started;
        if(started)
            System.out.println("Enginne started");
        else
            System.out.println("Engine stopped");
    }

    boolean isStarted(){
        return started;
    }
}
