package control.isp.agergare;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello!");
        Senzor s1 = new Senzor();
        //s1.v = -100; //eroare atribut private
        s1.setValoare(-100);
        s1.afiseaza();

        Senzor s2 = new Senzor(15,"UTCN");
        s2.afiseaza();

        Controler x1 = new Controler(s1);
        x1.control();
    }

}//.end



class Senzor{
    //atribute
    private int v;
    private String l;

    //constructori
    public Senzor() {
        this.v = 10;
        this.l = "Acasa";
    }

    public Senzor(int v, String l) {
        this.v = v;
        this.l = l;
    }

    //metode
    void afiseaza(){
        System.out.println("Senzor "+v+" "+l);
    }

    void setValoare(int v){

        if(v<0||v>100)
            System.out.println("Valoarea "+v+" este invalida");
        else
            this.v = v;
    }

    int getValoare(){
        return v;
    }


}//.end

class Controler{
    private Senzor s;

    public Controler(Senzor s) {
        this.s = s;
    }

    public void control(){
        System.out.println("Aplica logica de control...");
        if(s.getValoare()<50)
            System.out.println("Porneste caldura!");
        else
            System.out.println("Porneste aerul conditionat!");
    }
}





