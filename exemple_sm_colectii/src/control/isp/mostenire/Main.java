package control.isp.mostenire;


public class Main {
    public static void main(String[] args) {
        Persoana p1 = new Persoana("alin",19);
        Persoana p2 = new Persoana("ion",29);
        p1.afiseaza();
        p2.afiseaza();
        Student s1 = new Student("dan",21,"utcn");
        s1.afiseaza();
    }
}

class Persoana{
    private String nume;
    private int varsta;

    public Persoana(String nume, int varsta) {
        this.nume = nume;
        this.varsta = varsta;
    }

    public String getNume() {
        return nume;
    }

    public int getVarsta() {
        return varsta;
    }

    void afiseaza(){
        System.out.println("Persoana "+nume+" varsta "+varsta);
    }
}

class Student extends Persoana{
    private String universitate;

    public Student(String nume, int varsta, String universitate) {
        super(nume, varsta);
        this.universitate = universitate;
    }

    void afiseaza(){
        System.out.println("Student "+getNume()+" varsta "+getVarsta()+" "+universitate);
    }

}

